//
//  Message.swift
//  HeartMed
//
//  Created by Trevon Fernando on 11/23/15.
//  Copyright © 2015 Trevon Fernando. All rights reserved.
//

import UIKit

public enum messageType {
    case question
    case normal
    case answer
    case image
}

public class Message: NSObject {
    
    public var  text: String = ""
    public var  type:messageType  = .normal
    public var  question:Question = Question()
}
