//
//  DataViewController.swift
//  HeartMed
//
//  Created by Trevon Fernando on 11/27/15.
//  Copyright © 2015 Trevon Fernando. All rights reserved.
//

import UIKit
import Charts
import Parse

class DataViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {


    
    @IBOutlet weak var tableview: UITableView!
    var questions : [Question] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        tableview.tableFooterView = UIView()
    }
    
    override func viewWillAppear(animated: Bool) {
        getUserQuestions(User.currentUser()!)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCellWithIdentifier("PieGraphTableViewCellIdentifier", forIndexPath: indexPath) as!  PieGraphTableViewCell
        if questions.count > 0 {
            let question = questions[indexPath.row]
            cell.questionText.text = question.question
            let yesCounts = getAnsYesCounts().count
            let noCount = getAnsNoCounts().count
            let unAns = getAnsUnansCounts().count
            cell.setChart(["Yes" , "No" , "N/A"], values: [Double(yesCounts), Double(noCount) , Double(unAns)])
        }

        return cell
    }
    
    
    func getUserQuestions(user : User){
        
        let query = Question.query()
        query!.whereKey("user", equalTo:user)
        query!.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                let questionsObjs = (objects as? [Question])!
                print(questionsObjs.count)
                self.questions = questionsObjs
                self.tableview.reloadData()
            }
        }
    }
    
    func getAnsYesCounts()-> NSArray{
        return getPointValues("Yes")
    }
    
    func getAnsNoCounts()-> NSArray{
        return getPointValues("Not at all")
    }
    
    func getAnsUnansCounts()-> NSArray{
        return getPointValues("")
    }
    
    func getPointValues(answer : String) -> NSArray {
        var clusterQuestion : [Question] = []
        for question in questions {
            let ans : String = question.answer
            if (ans.lowercaseString  == answer.lowercaseString){
                clusterQuestion.append(question)
            }
        }
        return clusterQuestion
    }
}
