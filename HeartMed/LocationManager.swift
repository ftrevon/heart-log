//
//  LocationManager.swift
//  HeartMed
//
//  Created by Trevon Fernando on 11/8/15.
//  Copyright © 2015 Trevon Fernando. All rights reserved.
//

import CoreLocation
import UIKit

protocol LocationDelegate {
    func updateStepCount(activity : String ,count: Int)
}

class LocationManager : CLLocation , CLLocationManagerDelegate , MotionDelegate {
    
    
    var manager: CLLocationManager?
    let motionManager : MotionManager = MotionManager()
    var locationDelegate = LocationDelegate?()


    func getGetLocation (){
        manager = CLLocationManager()
        manager?.desiredAccuracy = kCLLocationAccuracyBest
        manager?.delegate = self
        manager?.requestAlwaysAuthorization()
        manager?.pausesLocationUpdatesAutomatically = false
        manager?.allowsBackgroundLocationUpdates = true
        manager?.startUpdatingLocation()

    }
    
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {

        
        if UIApplication.sharedApplication().applicationState == .Active {
            NSLog("App is active. New location is %@", newLocation)

        } else {
            NSLog("App is backgrounded. New location is %@", newLocation)
        }
        //Getting stepcount 
        motionManager.motionDelegate = self
        motionManager.getActivityState()
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        NSLog("LOCATION ERROR %@", error)

    }
    
    func locationManagerDidPauseLocationUpdates(manager: CLLocationManager) {
        print("LOCATION PAUSED !")
    }
    
    func locationManagerDidResumeLocationUpdates(manager: CLLocationManager) {
        print("LOCATION RESUMED !")

    }
        
    func stepCountAndActivity(activity : String , count: Int) {
        locationDelegate?.updateStepCount(activity, count: count)
    }
    
}
