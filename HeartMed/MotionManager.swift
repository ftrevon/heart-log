//
//  MotionManager.swift
//  HeartMed
//
//  Created by Trevon Fernando on 11/6/15.
//  Copyright © 2015 Trevon Fernando. All rights reserved.
//

import Foundation
import CoreMotion


public enum activityState {
    case Stationary
    case Walking
    case Running
    case Automotive
}

protocol MotionDelegate {
    func stepCountAndActivity(activity : String , count: Int)
}

class MotionManager {
    
    let activityManager = CMMotionActivityManager()
    let pedoMeter = CMPedometer()
    var motionDelegate = MotionDelegate?()

    
    func getActivityState () -> activityState {
        var state = activityState.Stationary
        if(CMMotionActivityManager.isActivityAvailable()){
            self.activityManager.startActivityUpdatesToQueue(NSOperationQueue.mainQueue()) { data in
                if let data = data {
                    dispatch_async(dispatch_get_main_queue()) {
                        if(data.stationary == true){
                            state = activityState.Stationary
                        } else if (data.walking == true){
                            state = activityState.Walking
                        } else if (data.running == true){
                            state = activityState.Running
                        } else if (data.automotive == true){
                            state = activityState.Automotive
                        }
                        self.motionDelegate?.stepCountAndActivity(String(activityState), count: self.getStepsCount())
                        print(String(state))
                   }
                }
            }
        }
        return state
    }
    
    func getStepsCount () -> Int {
        var stepCount = NSNumber()
        if(CMPedometer.isStepCountingAvailable()){
            let fromDate = NSDate(timeIntervalSinceNow: -60 * 24)
            self.pedoMeter.queryPedometerDataFromDate(fromDate, toDate: NSDate(), withHandler: { data , error in
                if (error == nil) {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                       stepCount = (data?.numberOfSteps)!
                       print(String(stepCount.integerValue))
                        self.motionDelegate?.stepCountAndActivity(String(self.getActivityState()), count: stepCount.integerValue)
                    })
                }
            })
        }
        return stepCount.integerValue
    }

}