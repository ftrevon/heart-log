//
//  NotificationManager.swift
//  HeartMed
//
//  Created by Trevon Fernando on 11/8/15.
//  Copyright © 2015 Trevon Fernando. All rights reserved.
//

import UIKit

class NotificationManager {
    
    
    func setupNotificationSettings() {
        // Specify the notification types.
        let notificationSettings: UIUserNotificationSettings! = UIApplication.sharedApplication().currentUserNotificationSettings()
        
        if (notificationSettings.types == UIUserNotificationType.None){
            let notificationTypes: UIUserNotificationType = UIUserNotificationType.Alert
            
            let yesNotificationAction = UIMutableUserNotificationAction()
            yesNotificationAction.identifier = "yesNotificationAction"
            yesNotificationAction.title = "Yes"
            yesNotificationAction.activationMode = UIUserNotificationActivationMode.Background
            yesNotificationAction.destructive = false
            yesNotificationAction.authenticationRequired = false
            
            let noNotificationAction = UIMutableUserNotificationAction()
            noNotificationAction.identifier = "noNotificationAction"
            noNotificationAction.title = "Not at all !"
            noNotificationAction.activationMode = UIUserNotificationActivationMode.Background
            noNotificationAction.destructive = false
            noNotificationAction.authenticationRequired = false
            
            let actionsArray = NSArray(objects: yesNotificationAction, noNotificationAction)
            
            let fatigueReminderCategory = UIMutableUserNotificationCategory()
            fatigueReminderCategory.identifier = "fatigueReminderCategory"
            fatigueReminderCategory.setActions(actionsArray as? [UIUserNotificationAction], forContext: UIUserNotificationActionContext.Default)
            
            let categoriesForSettings = NSSet(objects: fatigueReminderCategory)
            
            let newNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: categoriesForSettings as? Set<UIUserNotificationCategory>)
            UIApplication.sharedApplication().registerUserNotificationSettings(newNotificationSettings)
        }
        
    }
    
    func scheduleLocalNotification(question : Question) {
        let localNotification = UILocalNotification()
        localNotification.fireDate = NSDate()
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.alertBody = question.question + "<< slide left here >>"
        localNotification.alertAction = "Log it mate!"
        localNotification.category = "fatigueReminderCategory"
        let customData : [String: String] = ["questionID" : question.objectId!]
        localNotification.userInfo = customData
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }
}
