//
//  LogMessageTableViewCell.swift
//  HeartMed
//
//  Created by Trevon Fernando on 11/22/15.
//  Copyright © 2015 Trevon Fernando. All rights reserved.
//

import UIKit

class LogMessageTableViewCell: UITableViewCell {

    @IBOutlet weak var messageText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
