//
//  User.swift
//  HeartMed
//
//  Created by Trevon Fernando on 11/11/15.
//  Copyright © 2015 Trevon Fernando. All rights reserved.
//

import Parse

public class User: PFUser {
    
    @NSManaged public var  name: String
    override public class func initialize() {
        struct Static {
            static var onceToken : dispatch_once_t = 0;
        }
        dispatch_once(&Static.onceToken) {
            self.registerSubclass()
        }
    }
}
