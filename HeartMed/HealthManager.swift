//
//  HealthManager.swift
//  HeartMed
//
//  Created by Trevon Fernando on 9/15/15.
//  Copyright (c) 2015 Trevon Fernando. All rights reserved.
//

import Foundation

import HealthKit

class HealthManager {
 
    let healthKitStore:HKHealthStore = HKHealthStore()

    func authorizeHealthKit(completion: ((success:Bool, error:NSError!) -> Void)!)
    {
        // 1. Set the types you want to read from HK Store
        let healthKitTypesToRead = Set(arrayLiteral:
            HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierDateOfBirth)!,
            HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierBloodType)!,
            HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierBiologicalSex)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!,
            HKObjectType.workoutType()
            )
        
        // 2. Set the types you want to write to HK Store
        let healthKitTypesToWrite = Set(arrayLiteral:
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMassIndex)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!,
            HKQuantityType.workoutType()
            )
        
        // 3. If the store is not available (for instance, iPad) return an error and don't go on.
        if !HKHealthStore.isHealthDataAvailable()
        {
            let error = NSError(domain: "heartmed.com", code: 2, userInfo: [NSLocalizedDescriptionKey:"HealthKit is not available in this Device"])
            if( completion != nil )
            {
                completion(success:false, error:error)
            }
            return;
        }
        
        // 4.  Request HealthKit authorization
        healthKitStore.requestAuthorizationToShareTypes(healthKitTypesToWrite, readTypes: healthKitTypesToRead) { (success, error) -> Void in
            
            if( completion != nil )
            {
                completion(success:success,error:error)
            }
        }
    }
    
    func readProfile() -> ( age:Int?,  biologicalsex:HKBiologicalSexObject?, bloodtype:HKBloodTypeObject?)
    {
        var error:NSError?
        var age:Int?
        
        // 1. Request birthday and calculate age
        let birthDay = try? healthKitStore.dateOfBirth()
        
        if ((birthDay) != nil)
        {
            let today = NSDate()
            let calendar = NSCalendar.currentCalendar()
           // let differenceComponents = NSCalendar.currentCalendar().components(.CalendarUnitYear, fromDate: birthDay, toDate: today, options: NSCalendarOptions(0) )
            let differenceComponents = NSCalendar.currentCalendar().components(.Year, fromDate: birthDay!, toDate: today, options: NSCalendarOptions(rawValue: 0))
            age = differenceComponents.year
        }
        if error != nil {
            print("Error reading Birthday: \(error)")
        }
        
        // 2. Read biological sex
        let biologicalSex:HKBiologicalSexObject? = try? healthKitStore.biologicalSex();
        if error != nil {
            print("Error reading Biological Sex: \(error)")
        }
        // 3. Read blood type
        let bloodType:HKBloodTypeObject? = try? healthKitStore.bloodType();
        if error != nil {
            print("Error reading Blood Type: \(error)")
        }
        
        // 4. Return the information read in a tuple
        return (age, biologicalSex, bloodType)
    }
    
    
    func readStepCount() {
        
        let sampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)
        self.enableBackgroundUpdates(sampleType!)

        let query = HKObserverQuery(sampleType: sampleType!, predicate: nil) {
            query, completionHandler, error in
            
            if error != nil {
                // Perform Proper Error Handling Here...
                print("*** An error occured while setting up the stepCount observer. \(error!.localizedDescription) ***")
                abort()
            }
            
            
            // Take whatever steps are necessary to update your app's data and UI
            // This may involve executing other queries
            self.updateDailyStepCount()
            // If you have subscribed for background updates you must call the completion handler here.
             completionHandler()
        }
        
        healthKitStore.executeQuery(query)
    }
    
    func updateDailyStepCount (){
        print("stepcount updated")
        let stepSampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)
        
        let endDate = NSDate()
//        let startDate = NSCalendar.currentCalendar().dateByAddingUnit(.NSMonthCalendarUnit, value: -1, toDate: endDate, options: nil)
       // let startDate = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: -1, toDate: endDate, options: .MatchFirst)
        let fromDate = NSDate(timeIntervalSinceNow: -60 * 24)

        let predicate = HKQuery.predicateForSamplesWithStartDate(fromDate, endDate: endDate, options: .None)
        let query = HKSampleQuery(sampleType: stepSampleType!, predicate: predicate, limit: 0, sortDescriptors: nil, resultsHandler: {
            (query, results, error) in
            if results == nil {
                print("There was an error running the query: \(error)")
            }
            
            dispatch_async(dispatch_get_main_queue()) {
                var dailyAVG:Double = 0
                for steps in results as! [HKQuantitySample]
                {
                    // add values to dailyAVG
                    dailyAVG += steps.quantity.doubleValueForUnit(HKUnit.countUnit())
                    print(dailyAVG)
                    print(steps)
                }
            }
        })
        
        healthKitStore.executeQuery(query)


    }
    
    func enableBackgroundUpdates (object :HKObjectType) {
        healthKitStore.enableBackgroundDeliveryForType(object, frequency:.Immediate) { (success, error) -> Void in
            if success {
                print("background data enables")
                
            }
        }
    }
    
    
}