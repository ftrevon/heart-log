//
//  AppDelegate.swift
//  HeartMed
//
//  Created by Trevon Fernando on 9/15/15.
//  Copyright (c) 2015 Trevon Fernando. All rights reserved.
//

import UIKit
import Parse
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        Fabric.with([Crashlytics()])

        // Override point for customization after application launch.
        Parse.enableLocalDatastore()
        Parse.setApplicationId("ZZhMDwsLIMz11705sgZ6isloghTIpaIed0k38ZYJ", clientKey: "yyAnaTxSVfi7ZatVnYw05JdMQ6qarh698fXv05Y3")
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forLocalNotification notification: UILocalNotification, completionHandler: () -> Void) {
     
        let query = Question.query()
        query!.getObjectInBackgroundWithId(notification.userInfo!["questionID"]! as! String){
            (question: PFObject?, error: NSError?) -> Void in
            if error == nil {
                if let  firstQuestion = question {
                    if identifier == "yesNotificationAction" {
                        NSNotificationCenter.defaultCenter().postNotificationName("yesNotification", object: firstQuestion)
                    }
                    else if identifier == "noNotificationAction" {
                        NSNotificationCenter.defaultCenter().postNotificationName("noNotification", object: firstQuestion)
                    }
                    completionHandler()
                }
            }
        }
    }
}

