//
//  ViewController.swift
//  HeartMed
//
//  Created by Trevon Fernando on 9/15/15.
//  Copyright (c) 2015 Trevon Fernando. All rights reserved.
//

import UIKit
import HealthKit


class ViewController: UIViewController, LocationDelegate , UITableViewDataSource , UITableViewDelegate{

    let kUnknownString   = "Unknown"

    @IBOutlet weak var tableview: UITableView!

    let healthManager:HealthManager = HealthManager()
    let motionManafer : MotionManager = MotionManager()
    let locationManager:LocationManager = LocationManager()
    let notificationManager:NotificationManager = NotificationManager()
    
    var fatigueNotificationSent : Bool = false
    var inActive : Bool = true

    var animatedCells : Bool = false
    var mainQuestion : Question!
    var timer       = NSTimer()


    var messages : [Message] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        notificationManager.setupNotificationSettings()
        //authorizeHealthKit()
       // let profile = self.healthManager.readStepCount()

        //weightLabel.text = self.bloodTypeLiteral(profile.bloodtype?.bloodType)
        locationManager.locationDelegate = self
        locationManager.getGetLocation()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleYesNotification:", name: "yesNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleNoNotification:", name: "noNotification", object: nil)
        
        tableview.estimatedRowHeight = 40.0
        tableview.rowHeight = UITableViewAutomaticDimension
        
        if(NSUserDefaults.standardUserDefaults().boolForKey("HasLaunchedOnce"))
        {
            // app already launched
            
        }
        else
        {
            let message : Message = Message()
            message.type = messageType.normal
            message.text = "Hi there , Welcome to Heart Log!"
            messages.append(message)
            
            let message1 : Message = Message()
            message1.type = messageType.normal
            message1.text = "Our goal is help you keep track of how you feel after physical activities"
            messages.append(message1)
            
            let message2 : Message = Message()
            message2.type = messageType.normal
            message2.text = "The more you tell us, the more we can help you"
            messages.append(message2)
            // This is the first launch ever
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "HasLaunchedOnce")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        if animatedCells == false {
            animateTable()
            animatedCells = true
        }
    }
    
    func animateTable() {
        tableview.reloadData()
        
        let cells = tableview.visibleCells
        let tableHeight: CGFloat = tableview.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransformMakeTranslation(-tableHeight, 0)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animateWithDuration(1.5, delay: 1.0 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .AllowAnimatedContent, animations: {
                cell.transform = CGAffineTransformMakeTranslation(0, 0);
                }, completion: nil)
            
            index += 1
        }
    }
    

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return messages.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        let  cell = tableView.dequeueReusableCellWithIdentifier("LogMessageIdentifier", forIndexPath: indexPath) as!  LogMessageTableViewCell
        if message.type == messageType.answer {
            let cell = tableView.dequeueReusableCellWithIdentifier("AnsMessageIdentifier", forIndexPath: indexPath) as!  AnsMessageTableViewCell
           cell.answerText.text = message.text
            return cell
        }else{
            cell.messageText.text = messages[indexPath.row].text
            if message.type == messageType.question {
                mainQuestion = message.question
            }
        }
        return cell
    }
    
    func authorizeHealthKit(){
        healthManager.authorizeHealthKit { (authorized,  error) -> Void in
            if authorized {
                print("HealthKit authorization received.")

            }
            else
            {
                print("HealthKit authorization denied!")
                if error != nil {
                    print("\(error)")
                }
            }
        }
    }
    
    func fireNotification (){
 
        timer.invalidate()
        let question = Question()
        question.question = "Hey, you walked like an eygptian, do you feel any fatigue now ?"
        question.user = User.currentUser()!
       // question.activity = activity
        question.saveInBackgroundWithBlock({ (sucess, error) -> Void in
            self.notificationManager.scheduleLocalNotification(question)
        })
        
        
        let message : Message = Message()
        message.type = messageType.question
        message.text = question.question
        message.question = question
        
        self.messages.append(message)
        self.tableview.reloadData()
     


    }
    
    func updateStepCount(activity: String, count: Int) {
        if fatigueNotificationSent == false{
//            if count > 1 && activity == "Walking"{
            if count > 90{
                
                if activity == "Stationary" {
                    self.fatigueNotificationSent = true
                    inActive = false
                    timer = NSTimer.scheduledTimerWithTimeInterval(120.0, target: self, selector: "fireNotification", userInfo: activity, repeats: false)
                }


            }else{
                self.fatigueNotificationSent = false
                if inActive == true {
                    inActive = false
                    let message : Message = Message()
                    message.type = messageType.normal
                    message.text = "Looks like you havent gone for a long walk in the past hour."
                    self.messages.append(message)
                    self.tableview.reloadData()
            
                }
            }
        }
    }


    func bloodTypeLiteral(bloodType:HKBloodType?)->String
    {
        
        if bloodType != nil {
            
            switch( bloodType! ) {
            case .ABNegative:
                return  "A+"
            case .ANegative:
                return "A-"
            case .BPositive:
                return "B+"
            case .BNegative:
                return "B-"
            case .ABPositive:
                return "AB+"
            case .ABNegative:
                return "AB-"
            case .OPositive:
                return "O+"
            case .ONegative:
                return "O-"
            default:
                break;
            }
            
        }
        return "Unknow";
    }
    
    //MARK : Notifications

    func handleYesNotification(notification : NSNotification) {
        if (notification.object != nil) {
            let question : Question = notification.object as! Question
            question.answer = "yes"
            question.saveInBackground()
        }
    }
    
    
    //MARK : Answers Buttons    
    @IBAction func yesAnsAction(sender: AnyObject) {
        if((mainQuestion) != nil){
            let message : Message = Message()
            message.type = messageType.answer
            message.text = "Yes"
            self.messages.append(message)
            self.tableview.reloadData()
            
            mainQuestion.answer = "Yes"
            mainQuestion.saveInBackground()
            mainQuestion = nil

        }

    }
    
    @IBAction func noAnsAction(sender: AnyObject) {
        if((mainQuestion) != nil){
        let message : Message = Message()
        message.type = messageType.answer
        message.text = "Not at all"
        self.messages.append(message)
        self.tableview.reloadData()
            
        mainQuestion.answer = "Not at all"
        mainQuestion.saveInBackground()
        mainQuestion = nil
        }
    }
}

