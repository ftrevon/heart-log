//
//  WelcomeViewController.swift
//  HeartMed
//
//  Created by Trevon Fernando on 11/22/15.
//  Copyright © 2015 Trevon Fernando. All rights reserved.
//

import UIKit
import Parse

class WelcomeViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        if (User.currentUser() != nil) {
                self.performSegueWithIdentifier("overview", sender: self)
        }
    }
    
    @IBAction func doneAction(sender: AnyObject) {
        let email = self.emailTextField.text
        let finalEmail = email!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        let userObj = User()
        userObj.email = finalEmail
        userObj.username = finalEmail
        userObj.password = finalEmail
        
        userObj.signUpInBackgroundWithBlock { (succeeded: Bool, error: NSError?) -> Void in
        if let error = error {
        let errorString = error.userInfo["error"] as? NSString
        // Show the errorString somewhere and let the user try again.
            print(errorString)
        } else {
        print("YES")
        // Hooray! Let them use the app now.
            self.performSegueWithIdentifier("overview", sender: self)

        }
        }

    }
}
