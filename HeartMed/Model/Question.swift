//
//  Question.swift
//  HeartMed
//
//  Created by Trevon Fernando on 11/17/15.
//  Copyright © 2015 Trevon Fernando. All rights reserved.
//

import Parse

public class Question: PFObject , PFSubclassing {
    
    @NSManaged public var  user: PFUser
    @NSManaged public var  question: String
    @NSManaged public var  answer: String
    @NSManaged public var  activity: String

    
    //Converstation
    //Query question for the user and check which questions are not answered
    //check activity type and make the questions
    
    //Example : Q: You moved like an egyptian and activity is walking . Hey "name", looks like you went on a special walk. , how did you feel after the walk ? 
    
    
    override public class func initialize() {
        struct Static {
            static var onceToken : dispatch_once_t = 0;
        }
        dispatch_once(&Static.onceToken) {
            self.registerSubclass()
        }
    }
    
    public static func parseClassName() -> String {
        return "Question"
    }

}
